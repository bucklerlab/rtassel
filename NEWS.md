# rTASSEL 0.9.26
* Bug fixes:
  + Fixed `r2` parameter bug in `ldPlot()`
  + Fixed space bugs in certain column names of data frame objects. 
    `_` values now replace spaces.
  + Fixed `show()` method for `TasselDistanceMatrix` objects.
* Add new function:
  + `seqDiversity()`
  + Calculates diversity basic diversity metrics on genetic data.


# rTASSEL 0.9.25
* Bug fixes:
  + Fixed character conversion bug in `DataFrame` object returns.
* `pca()` can optionally report eigenvalues and eigenvectors as a list object.
* Added new function:
  + `imputeNumeric()`
  + Allows for numeric imputation of `GenotypeTable` objects.
* Added new function:
  + `imputLDKNNi()`
  + Allows for LD KNNi imputation of `GenotypeTable` objects.


# rTASSEL 0.9.24
* Added new function:
  + `pca()`
  + Allows for user to run PCA on rTASSEL objects containing a `GenotypeTable`
    object.
* Added new function:
  + `mds()`
  + Allows for user to run MDS on `TasselDistanceMatrix` objects.
* Enhancements:
  + New summary print output for `TasselDistanceMatrix` objects.


# rTASSEL 0.9.23
* Added new `TasselDistanceMatrix` class
  + Specified function (`kinshipMatrix()` and `distanceMatrix()`) now return
    an object of type `TasselDistanceMatrix`.
  + Prevents console overload and freezing as seen with large distance matrix
    objects.
  + Now shows summary overview of matrix instead of Java object reference.
  + Generic functions `colnames()`, `rownames()`, `ncol()`, and `nrow()` will
    return relative information similar to how these operate with `matrix`
    type objects.
  + Primitive function `as.matrix()` now supersedes deprecated functions
    `kinshipToRMatrix()` and `distanceToRMatrix()`.
  + Prior functions that take in a kinship object will now take in this new
    class.
* Added new function:
  + `readTasselDistanceMatrix()`
  + Allows for user to read in delimited distance matrix stored in a flat
    file.
* Added new function:
  + `asTasselDistanceMatrix()`
  + Coerces a pairwise matrix (e.g. m x m dimensions) with the same column
    and row names of type `matrix` to an object of type `TasselDistanceMatrix`.
* Added new function:
  + `createTree()`
  + interface to TASSEL's tree creation methods
  + Allows for `Neighbor_Joining` and `UPGMA` methods
* Added new function:
  + `treeJavaApp()`
  + wrapper for TASSEL's interface to the Archaeopteryx Java tree Viewer
  + Implements same methods for tree creation as `createTree()`


# rTASSEL 0.9.22
* Fix `manhattanPlot()` aesthetics:
  + Remove redundant marker labels from x-axis
  + Change x-axis label to `SNP Positions`


# rTASSEL 0.9.21
* Added new function:
  + `exportGenotypeTable()`
* Added new vignette:
  + "Filtering Genotype Tables"


# rTASSEL 0.9.20
* Added new parameter to `filterGenotypeTableSites()`
  + `gRangesObj`: Filter genotype tables by using a `GRanges` object.
* Added new parameter to `filterGenotypeTableTaxa()`
  + `taxa`: Pass a vector of taxa IDs to filter genotype table by.
* Fixed `getSumExpFromGenotypeTable()` bug:
  + dosage array now returns `NA`s instead of `128` values.


# rTASSEL 0.9.19
* Added two new parameters to `filterGenotypeTableSites()`
  + `removeMinorSNPStates`: Boolean; removes minor SNP states.
  + `removeSitesWithIndels`: Boolean; removes sites with indels.
* Added better descriptive error handling for `filterGenotypeTableSites()`
* Fixed `siteRangeFilterType` parameter bug in `filterGenotypeTableSites()`. 
  Now defaults to `none` when user does not specify filter type.
* Added two new parameters to `getSumExpFromGenotypeTable()`
  + `coerceDosageToInt`: Returns raw byte dosage array instead of integer from Java.
  + `verbose`: Display console messages for large "memory-intensive" datasets.


# rTASSEL 0.9.18
* Added functions to calculate linkage disequilibrium (LD)
* Proposed LD functions:
  + `linkageDiseq()` - Returns TASSEL LD table report as data frame
  + `ldPlot()` - Returns static `ggplot2` plot
  + `ldJavaApp()` - Initiates TASSEL's interactive LD viewer


# rTASSEL 0.9.17
* Added new function:
  + `manhattanPlot()`
* Removed tidyverse dependencies


# rTASSEL 0.9.16
* Added write to file parameters for:
  + assocModelFitter()
* Added p-value threshold parameters for:
  + assocModelFitter()
* Added thread usage paramters for:
  + assocModelFitter()
* Optimized table report to data frame generation
* Added new filtration features for genotype tables via filterGenotypeTableSites()
  + parameters for variant sites
  + parameters for physical positions
  + filtration via chromsomome position files
  + filtration via BED file formats


# rTASSEL 0.9.13
* Added error checks for catching C stack usage errors for the following functions:
  + filterGenotypeTableSites()
  + filterGenotypeTableTaxa()
* Added NEWS file for tracking version updates.


# rTASSEL 0.9.12
* Added new functions:
  + `leaveOneFamilyOut()`
  + `genomicPredction()`
* Fixed a bug where tibbles when passed through `readPhenotypeFromDataFrame()`,
  would cause errors.
